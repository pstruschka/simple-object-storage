use iron::{typemap, BeforeMiddleware, IronResult, Request};
use mongodb::db::ThreadedDatabase;
use mongodb::{self, Client, ClientInner, CommandResult, ThreadedClient};
use object::Object;
use std::sync::Arc;

pub static DATABASE: &'static str = "meta";

pub struct MongoMiddleware {
    pub client: Client,
}

pub struct Value(Client);

impl typemap::Key for MongoMiddleware {
    type Value = Value;
}

impl MongoMiddleware {
    //#[allow(clippy::needless_pass_by_value)]
    fn log_query_duration(_client: Client, command_result: &CommandResult) {
        match command_result {
            CommandResult::Success {
                duration,
                ref reply,
                ref command_name,
                ..
            } => {
                debug!(
                    "Command {} took {} nanoseconds. Response: {}",
                    command_name, duration, reply
                );
            }
            _ => warn!("Failed to execute command."),
        }
    }

    pub fn new(path: &str, port: u16) -> mongodb::Result<MongoMiddleware> {
        let mut client: Arc<ClientInner> = Client::connect(path, port)?;
        client.add_completion_hook(MongoMiddleware::log_query_duration)?;
        Ok(MongoMiddleware { client })
    }

    pub fn get_connection(&self) -> Client {
        self.client.clone()
    }
}

impl BeforeMiddleware for MongoMiddleware {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions
            .insert::<MongoMiddleware>(Value(self.get_connection()));
        Ok(())
    }
}

pub trait MongoRequestExtension {
    fn database_connection(&self) -> &Client;
}

impl<'a, 'b> MongoRequestExtension for Request<'a, 'b> {
    fn database_connection(&self) -> &Client {
        let pv = self.extensions.get::<MongoMiddleware>().unwrap(); // Is this safe?
        let Value(ref poll) = pv;
        poll
    }
}

pub fn drop_object_metadata(object: &Object, client: &Client) -> Result<(), String> {
    let coll = client.db(DATABASE).collection(&object.bucket);
    match coll.delete_one(
        doc!{
            object.to_base64(): {"$exists": true}
        },
        None,
    ) {
        Ok(_) => Ok(()),
        Err(e) => Err(format!("{}", e)),
    }
}

pub fn drop_bucket_metadata(bucket: &str, client: &Client) -> Result<(), String> {
    let coll = client.db(DATABASE).collection(&bucket);
    match coll.drop() {
        Ok(_) => Ok(()),
        Err(e) => Err(format!("{}", e)),
    }
}
