extern crate crypto;
extern crate regex;

use iron::headers::Range;
use iron::request::Body;
use iron::status::{self, Status};
use serde_json::Value;

use mongodb::Client;
use object::drop_object;
use object::{complete_object, download_object, drop_part, new_lock_file, save_part};
use object::{CompleteObject, Object, Part};

pub fn create_ticket(object: &Object) -> Status {
    match new_lock_file(object) {
        Err(e) => {
            warn!("{}", e);
            status::BadRequest
        }
        Ok(_) => status::Ok,
    }
}

pub fn upload_part(part: &Part, body: &mut Body) -> (Value, Status) {
    match save_part(part, body) {
        Err(e) => {
            warn!("{}", e);
            (json!({ "error": e }), status::BadRequest)
        }
        Ok((v, s)) => (v, s),
    }
}

pub fn delete_part(part: &Part) -> Status {
    match drop_part(part) {
        Err(e) => {
            warn!("{}", e);
            status::BadRequest
        }
        Ok(_) => status::Ok,
    }
}

pub fn delete_object(object: &Object, client: &Client) -> Status {
    match drop_object(object, client) {
        Err(e) => {
            warn!("{}", e);
            status::InternalServerError
        }
        Ok(s) => s,
    }
}

pub fn complete_ticket(object: &CompleteObject) -> (Value, Status) {
    match complete_object(object) {
        Err(e) => {
            warn!("{}", e);
            (json!({ "error": e }), status::InternalServerError)
        }
        Ok((v, s)) => (v, s),
    }
}

pub fn download_range(object: &Object, range: &Range, is_head: bool) -> (Option<Box<[u8]>>, usize, String, Status) {
    match download_object(object, range, is_head) {
        Ok((t, size, e_tag)) => (t, size, e_tag, status::Ok),
        Err(e) => {
            warn!("{}", e);
            (None, 0, "".to_string(), status::NotFound)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_ticket_fail() {
        let object: Object = Object {
            bucket: "bucket".to_string(),
            name: "name".to_string(),
        };
        assert_eq!(create_ticket(&object), status::BadRequest);
    }
}
