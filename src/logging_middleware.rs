extern crate url;

use self::url::Url;
use iron::headers::From;
use iron::BeforeMiddleware;
use iron::{IronError, IronResult, Request};

pub struct LogPathHandler;

impl LogPathHandler {
    fn log(&self, req: &mut Request) -> IronResult<()> {
        let method = &req.method;
        let url: Url = req.url.clone().into();
        let path = url.path();
        let query = match url.query() {
            None => "".to_string(),
            Some(q) => format!("{}{}", "?", q),
        };
        let source = match req.headers.get::<From>() {
            None => "".to_string(),
            Some(s) => format!(" - from: {}", s),
        };
        info!("{} {}{} {}", method, path, query, source);
        Ok(())
    }
}

impl BeforeMiddleware for LogPathHandler {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        self.log(req)
    }

    fn catch(&self, _req: &mut Request, err: IronError) -> IronResult<()> {
        Err(err)
    }
}
