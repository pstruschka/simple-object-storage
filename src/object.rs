extern crate base64;
extern crate bincode;
extern crate crypto;
extern crate fs2;
extern crate regex;

use self::bincode::{deserialize, deserialize_from, serialize_into};
use self::crypto::digest::Digest;
use self::crypto::md5::Md5;
use self::fs2::FileExt;
use self::regex::Regex;
use iron::headers::{ByteRangeSpec, Range};
use iron::request::Body;
use iron::status::{self, Status};
use serde_json::Value;
use std::cmp::Ordering;
use std::fmt::{self, Display};
use std::fs::{metadata, remove_file, OpenOptions};
use std::io::{BufWriter, Read, Seek, SeekFrom, Write};
use std::mem;
use std::path::{Path, PathBuf};
use std::str;
use std::time::UNIX_EPOCH;

use bucket::bucket_exists;
use bucket::update_bucket_modified;
use mongo_middleware::drop_object_metadata;
use mongodb::Client;

static FS_ROOT_DIR: &'static str = "./store/";

static DIRTY_BIT_MASK: u64 = (1 << 63);
const FILE_OFFSET: usize = mem::size_of::<LockFilePart>();

#[repr(align(8))]
#[derive(Copy, Serialize, Deserialize, Debug)]
pub struct FileHeader {
    pub number_of_parts: u64, // 8 bytes
    pub size: usize,          // 8 bytes
    pub md5: [u8; 16],        // 16 bytes
    pub created: u64,
    pub modified: u64,
}

impl FileHeader {
    pub fn incr(self: &mut Self) {
        self.number_of_parts += 1;
    }

    pub fn decr(self: &mut Self) {
        self.number_of_parts -= 1;
    }
}

impl Clone for FileHeader {
    fn clone(&self) -> FileHeader {
        *self
    }
}

#[repr(align(8))]
#[derive(Copy, Serialize, Deserialize, Eq, Debug)]
pub struct LockFilePart {
    pub part_number: u64, // MSB used as dirty bit, could be u16 but u64 to allow bincode to serialise correctly with 8 bytes padding
    part_size: usize,     // 8 bytes
    md5: [u8; 16],        // 16 bytes
}

impl Ord for LockFilePart {
    fn cmp(&self, other: &LockFilePart) -> Ordering {
        self.part_number.cmp(&other.part_number)
    }
}

impl PartialOrd for LockFilePart {
    fn partial_cmp(&self, other: &LockFilePart) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for LockFilePart {
    fn eq(&self, other: &LockFilePart) -> bool {
        self.part_number == other.part_number
    }
}

impl Clone for LockFilePart {
    fn clone(&self) -> LockFilePart {
        *self
    }
}

#[repr(align(8))]
#[derive(Copy, Serialize, Deserialize, Debug)]
pub struct CompletedFilePart {
    pub part_number: u64,
    part_size: usize,
    part_start: usize,
    part_end: usize,
}

impl Clone for CompletedFilePart {
    fn clone(&self) -> CompletedFilePart {
        *self
    }
}

pub struct Object {
    pub bucket: String,
    pub name: String,
}

impl Object {
    pub fn to_base64(self: &Self) -> String {
        let s = self.to_string();
        base64::encode(&s)
    }

    pub fn is_complete(self: &Self) -> bool {
        Object::complete_file_exists(self)
    }

    pub fn is_incomplete(self: &Self) -> bool {
        Object::lock_file_exists(self)
    }

    pub fn exists(self: &Self) -> bool {
        Object::lock_file_exists(self) || Object::complete_file_exists(self)
    }

    pub fn contains_part(self: &Self, part_number: u16) -> Result<bool, String> {
        if self.is_incomplete() {
            let mut f = try(OpenOptions::new()
                .read(true)
                .open(Object::get_lock_path(self)))?;
            let metadata = try(metadata(Object::get_lock_path(self)))?;
            let size = metadata.len() as usize;
            let header: FileHeader = try(deserialize_from(&f))?;
            let num_parts = header.number_of_parts;
            if num_parts == 0 {
                return Ok(false);
            }

            let mut read: usize = 0;
            let mut buf = [0u8; FILE_OFFSET * 128];
            let buf = &mut buf[..];

            let mut current_offset = mem::size_of::<FileHeader>();

            while current_offset < size {
                if read % FILE_OFFSET != 0 {
                    try(f.seek(SeekFrom::Current(-(read as i64 % FILE_OFFSET as i64))))?;
                }
                read = try(f.read(buf))?;
                for chunk in buf.chunks(FILE_OFFSET) {
                    let current_part = try(deserialize::<LockFilePart>(chunk))?;
                    if current_part.part_number == u64::from(part_number) {
                        try(f.unlock())?;
                        return Ok(true);
                    }
                    current_offset += FILE_OFFSET;
                }
            }

            Ok(false)
        } else {
            Ok(false)
        }
    }

    pub fn get_path(self: &Self) -> Option<PathBuf> {
        if Object::complete_file_exists(self) {
            Some(Object::get_complete_path(self))
        } else if Object::lock_file_exists(self) {
            Some(Object::get_lock_path(self))
        } else {
            None
        }
    }

    pub fn get_complete_path(object: &Self) -> PathBuf {
        let path = format!(
            "{}{}/completed/{}.completed",
            FS_ROOT_DIR, object.bucket, object.name
        ).to_string();
        PathBuf::from(path)
    }

    pub fn get_lock_path(object: &Self) -> PathBuf {
        let path = format!("{}{}/.{}.lock", FS_ROOT_DIR, object.bucket, object.name).to_string();
        PathBuf::from(path)
    }

    fn lock_file_exists(object: &Self) -> bool {
        let path = Object::get_lock_path(object);
        path.is_file()
    }

    fn complete_file_exists(object: &Self) -> bool {
        let path = Object::get_complete_path(object);
        path.is_file()
    }
}

impl Display for Object {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/{}", self.bucket, self.name)
    }
}

pub struct Part {
    pub object: Object,
    pub number: u64,
    pub size: usize,
    pub md5: [u8; 16],
}

impl Part {
    pub fn get_path(self: &Self) -> PathBuf {
        let part_file_string = format!(
            "{}{}/.{}.part{}",
            FS_ROOT_DIR, self.object.bucket, self.object.name, self.number
        );
        PathBuf::from(part_file_string)
    }
}

pub struct CompleteObject {
    pub object: Object,
    pub total_length: usize,
    pub e_tag: String,
}

pub fn new_lock_file(object: &Object) -> Result<(), String> {
    validate_bucket(&object.bucket)?;
    validate_object(&object)?;
    if object.exists() {
        return Err("Object exists".to_string());
    }
    let path = Object::get_lock_path(object);
    let f = try(OpenOptions::new().write(true).create_new(true).open(&path))?;
    let modified = get_modified_time(path)?;

    let header = FileHeader {
        number_of_parts: 0,
        size: 0,
        md5: [0; 16],
        created: modified,
        modified,
    };
    try(serialize_into(f, &header))?;
    Ok(())
}

fn commit_part(part: &Part) -> Result<(), String> {
    let path = part.object.get_path().unwrap();
    let mut f = try(OpenOptions::new().write(true).read(true).open(&path))?;
    let metadata = try(metadata(&path))?;
    let size = metadata.len() as usize;
    let modified = get_modified_time(path)?;

    try(f.lock_exclusive())?;

    // rewrite header
    let mut header: FileHeader = try(deserialize_from(&f))?;
    let num_parts = header.number_of_parts;
    header.incr();
    header.modified = modified;
    try(f.seek(SeekFrom::Start(0)))?;
    try(serialize_into(&f, &header))?;

    let mut buf = [0u8; FILE_OFFSET * 128];
    let buf = &mut buf[..];

    let lock_part = LockFilePart {
        part_number: part.number,
        part_size: part.size,
        md5: part.md5,
    };

    if num_parts == ((size - mem::size_of::<FileHeader>()) / FILE_OFFSET) as u64 {
        // if number of parts matches the file size append the new part to end of file
        try(f.seek(SeekFrom::End(0)))?;
        try(serialize_into(&f, &lock_part))?;
        try(f.unlock())?;
        Ok(())
    } else {
        // otherwise find the first dirty part and replace
        let mut read: usize = 0;

        let mut current_offset = mem::size_of::<FileHeader>();
        while current_offset < size {
            if read % FILE_OFFSET != 0 {
                try(f.seek(SeekFrom::Current(-(read as i64 % FILE_OFFSET as i64))))?;
            }
            //info!("read: {}", read);
            read = try(f.read(buf))?;
            for chunk in buf.chunks(FILE_OFFSET) {
                let mut current_part = try(deserialize::<LockFilePart>(chunk))?;
                if current_part.part_number & DIRTY_BIT_MASK == DIRTY_BIT_MASK {
                    try(f.seek(SeekFrom::Start(current_offset as u64)))?;
                    try(serialize_into(&f, &lock_part))?;
                    try(f.unlock())?;
                    return Ok(());
                }
                current_offset += FILE_OFFSET;
            }
        }

        Err("failed to commit part".to_string())
    }
}

pub fn save_part(part: &Part, body: &mut Body) -> Result<(Value, Status), String> {
    if !bucket_exists(&part.object.bucket) {
        return Ok((
            json!({
                "md5": to_hex_string(&part.md5),
                "length": part.size,
                "partNumber": part.number,
                "error": "InvalidBucket"
            }),
            status::BadRequest,
        ));
    } else if !object_name_valid(&part.object.name) {
        return Ok((
            json!({
                "md5": to_hex_string(&part.md5),
                "length": part.size,
                "partNumber": part.number,
                "error": "InvalidObjectName"
            }),
            status::BadRequest,
        ));
    } else if !part_number_in_range(part.number) {
        return Ok((
            json!({
                "md5": to_hex_string(&part.md5),
                "length": part.size,
                "partNumber": part.number,
                "error": "InvalidPartNumber"
            }),
            status::BadRequest,
        ));
    } else if !part.object.is_incomplete() {
        return Ok((
            json!({
                    "md5": to_hex_string(&part.md5),
                    "length": part.size,
                    "partNumber": part.number,
                    "error": "InvalidObjectName"
                }),
            status::BadRequest,
        ));
    }

    if part.object.contains_part(part.number as u16)? {
        return Ok((
            json!({
                    "md5": to_hex_string(&part.md5),
                    "length": part.size,
                    "partNumber": part.number,
                    "error": "PartExists"
                }),
            status::BadRequest,
        ));
    }

    let f = try(OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(part.get_path()))?;

    let mut writer = BufWriter::new(f);

    let mut buf = vec![0u8; 1024 * 1024 * 512];
    //let mut buf = [0u8; 1024 * 1024];
    let mut bytes_written: usize = 0;
    let mut md5 = Md5::new();

    while bytes_written < part.size {
        let bytes = try(body.read(&mut buf))?;
        info!("READ {}", bytes);
        let (to_write, _) = buf.split_at(bytes);
        try(writer.write(&to_write))?;
        md5.input(&to_write);
        bytes_written += bytes;
        if bytes == 0 {
            break;
        }
    }

    if bytes_written == 0 {
        return Ok((
            json!({
                    "md5": to_hex_string(&part.md5),
                    "length": part.size,
                    "partNumber": part.number,
                    "error": "NoData"
                }),
            status::BadRequest,
        ));
    }

    info!("md5: {}", md5.result_str());
    let mut md = [0u8; 16];
    let md = &mut md[..];
    md5.result(md);
    if bytes_written != part.size {
        //remove_part_file(&part)?;
        Ok((
            json!({
                    "md5": md5.result_str(),
                    "length": bytes_written,
                    "partNumber": part.number,
                    "error": "LengthMismatched"
                }),
            status::BadRequest,
        ))
    } else if md != part.md5 {
        //remove_part_file(&part)?;
        Ok((
            json!({
                    "md5": md5.result_str(),
                    "length": bytes_written,
                    "partNumber": part.number,
                    "error": "MD5Mismatched"
                }),
            status::BadRequest,
        ))
    } else {
        commit_part(&part)?;
        Ok((
            json!({
                "md5": md5.result_str(),
                "length": bytes_written,
                "partNumber": part.number
            }),
            status::Ok,
        ))
    }
}

pub fn drop_part(part: &Part) -> Result<(), String> {
    if !part_exists(&part) {
        return Err("Part file does not exist".to_string());
    }
    let path = part.object.get_path().unwrap();
    let mut f = try(OpenOptions::new().write(true).read(true).open(&path))?;
    let metadata = try(metadata(&path))?;
    let size = metadata.len() as usize;
    let modified = get_modified_time(path)?;

    try(f.lock_exclusive())?;

    // rewrite header
    // decrement number of parts
    let mut header: FileHeader = try(deserialize_from(&f))?;
    header.decr();
    header.modified = modified;
    try(f.seek(SeekFrom::Start(0)))?;
    try(serialize_into(&f, &header))?;

    // buffer for parts
    let mut buf = [0u8; FILE_OFFSET * 128];
    let buf = &mut buf[..];

    let read: usize = 0;

    let mut current_offset = mem::size_of::<FileHeader>();

    // loop through the lockfile looking for the part to mark dirty
    while current_offset < size {
        if read % FILE_OFFSET != 0 {
            try(f.seek(SeekFrom::Current(-(read as i64 % FILE_OFFSET as i64))))?;
        }
        let read = try(f.read(buf))?;
        let (bytes, _) = buf.split_at(read - (read % FILE_OFFSET));

        for win in bytes.chunks(FILE_OFFSET) {
            let mut current_part = try(deserialize::<LockFilePart>(win))?;
            if current_part.part_number == part.number {
                try(f.seek(SeekFrom::Start(current_offset as u64)))?;
                current_part.part_number ^= DIRTY_BIT_MASK;
                try(serialize_into(&f, &current_part))?;
                // Could remove part file but not necessary as `save_part` truncates the old part
                try(f.unlock())?;
                return Ok(());
            }
            current_offset += FILE_OFFSET;
        }
    }
    try(f.unlock())?;
    Err("Failed to find part".to_string())
}

pub fn drop_object(object: &Object, client: &Client) -> Result<Status, String> {
    if !object_name_valid(&object.name) {
        info!("BadObjectName");
        Ok(status::BadRequest)
    } else if object.is_complete() {
        let path = object.get_path().unwrap();
        // remove the object's complete file
        try(remove_file(path))?;
        drop_object_metadata(object, client)?;
        Ok(status::Ok)
    } else if object.is_incomplete() {
        remove_lock_file(object)?;
        Ok(status::Ok)
    } else {
        Ok(status::Ok)
    }
}

pub fn complete_object(completed_object: &CompleteObject) -> Result<(Value, Status), String> {
    let object = &completed_object.object;
    if !bucket_exists(&object.bucket) {
        return Ok((
            json!({
                    "error": "InvalidBucket"
                }),
            status::BadRequest,
        ));
    }
    if !object.is_incomplete() {
        return Ok((
            json!({
                    "name": object.name,
                    "error": "InvalidObjectName"
                }),
            status::BadRequest,
        ));
    }
    let path = Object::get_lock_path(object);
    info!("complete");
    let mut f = try(OpenOptions::new().write(true).read(true).open(&path))?;
    let metadata = try(metadata(&path))?;
    let size = metadata.len() as usize;
    let modified = get_modified_time(path)?;

    try(f.lock_exclusive())?;

    let mut header: FileHeader = try(deserialize_from(&f))?;

    let num_parts = header.number_of_parts;

    let mut vec: Vec<LockFilePart> = Vec::with_capacity(num_parts as usize);

    let mut buf = [0u8; FILE_OFFSET * 128];
    let buf = &mut buf[..];
    let read: usize = 0;

    let mut current_offset = mem::size_of::<FileHeader>();
    while current_offset < size {
        if read % FILE_OFFSET != 0 {
            try(f.seek(SeekFrom::Current(-(read as i64 % FILE_OFFSET as i64))))?;
        }
        let read = try(f.read(buf))?;
        let (bytes, _) = buf.split_at(read - (read % FILE_OFFSET));

        for win in bytes.chunks(FILE_OFFSET) {
            let mut current_part = try(deserialize::<LockFilePart>(win))?;
            if current_part.part_number & DIRTY_BIT_MASK == 0 {
                vec.push(current_part);
            }
            current_offset += FILE_OFFSET;
        }
    }

    try(f.unlock())?;

    if vec.is_empty() {
        return Ok((
            json!({
                    "name": object.name,
                    "error": "InvalidObjectName"
                }),
            status::BadRequest,
        ));
    }

    vec.sort();

    let path = Object::get_complete_path(object);
    let mut f = try(OpenOptions::new()
        .write(true)
        .read(true)
        .create_new(true)
        .open(path))?;
    try(f.seek(SeekFrom::Start(mem::size_of::<FileHeader>() as u64)))?;

    let mut total_size: usize = 0;

    let mut md5 = Md5::new();

    for part in vec {
        md5.input(&part.md5);
        info!("{} {} {}", total_size, part.part_size, part.part_number);
        let completed_part = CompletedFilePart {
            part_number: part.part_number,
            part_size: part.part_size,
            part_start: total_size,
            part_end: total_size + part.part_size - 1,
        };

        try(serialize_into(&f, &completed_part))?;
        total_size += part.part_size;
    }
    try(f.seek(SeekFrom::Start(0)))?;

    header.size = total_size;
    header.modified = modified;
    md5.result(&mut header.md5);

    try(serialize_into(&f, &header))?;
    let e_tag = format!("{}-{}", md5.result_str(), num_parts);
    info!("ETAG: {}", e_tag);
    if e_tag != completed_object.e_tag {
        warn!("ETAG MISMATCHED {} != {}", e_tag, completed_object.e_tag)
        //        remove_complete_file(object)?;
        //        Ok((
        //            json!({
        //                    "eTag": e_tag,
        //                    "length": total_size,
        //                    "name": object.name,
        //                    "error": "MD5Mismatched"
        //                }),
        //            status::BadRequest,
        //        ))
        //        } else if total_size != completed_object.total_length {
        //            remove_lock_file(object)?;
        //            update_bucket_modified(&object.bucket, modified)?;
        //            Ok((
        //                json!({
        //                        "eTag": e_tag,
        //                        "length": total_size,
        //                        "name": object.name
        //                    }),
        //                status::Ok,
        //            ))
        //    } else {
        //        remove_lock_file(object)?;
        //        update_bucket_modified(&object.bucket, modified)?;
        //        Ok((
        //            json!({
        //                    "eTag": e_tag,
        //                    "length": total_size,
        //                    "name": object.name
        //                }),
        //            status::Ok,
        //        ))
    }
    remove_lock_file(object)?;
    update_bucket_modified(&object.bucket, modified)?;
    Ok((
        json!({
                    "eTag": e_tag,
                    "length": total_size,
                    "name": object.name
                }),
        status::Ok,
    ))
}

pub fn download_object(object: &Object, range: &Range, is_head: bool) -> Result<(Option<Box<[u8]>>, usize, String), String> {
    if !object.is_complete() {
        return Err("".to_string());
    }
    info!("Download");

    let path = object.get_path().unwrap();
    let mut f = try(OpenOptions::new().read(true).open(&path))?;
    let metadata = try(metadata(path))?;
    let size = metadata.len() as usize;

    let header: FileHeader = try(deserialize_from(&f))?;
    let num_parts = header.number_of_parts;
    let file_size = header.size;

    let e_tag = format!("{}-{}", to_hex_string(&header.md5), header.number_of_parts);

    let mut vec: Vec<CompletedFilePart> = Vec::with_capacity(num_parts as usize);

    let mut buf = [0u8; FILE_OFFSET * 128];
    let buf = &mut buf[..];

    let read: usize = 0;

    info!("size {}", size);

    let mut current_offset = mem::size_of::<FileHeader>();
    while current_offset < size {
        if read % FILE_OFFSET != 0 {
            try(f.seek(SeekFrom::Current(-(read as i64 % FILE_OFFSET as i64))))?;
        }
        let read = try(f.read(buf))?;
        let (bytes, _) = buf.split_at(read - (read % FILE_OFFSET));

        for win in bytes.chunks(FILE_OFFSET) {
            info!("window: {}", win.len());
            if win.len() == FILE_OFFSET {
                let current_part = try(deserialize::<CompletedFilePart>(win))?;
                vec.push(current_part);
                current_offset += FILE_OFFSET;
                info!("done at offset {}", current_offset);
            }
        }
    }

    let mut download = Vec::with_capacity(1024);
    let mut size = 0;

    match range {
        Range::Bytes(v) => {
            for byte_range in v {
                info!("{}", byte_range);
                size = get_download_range(object, file_size, byte_range, &mut download, &vec, is_head)?;
            }
        }
        Range::Unregistered(_unit, _set) => {
            return Err("Invalid Range".to_string());
        }
    }
    if is_head {
        Ok((None, size, e_tag))
    } else {
        Ok((Some(download.into_boxed_slice()), size, e_tag))
    }
}

fn get_download_range(
    object: &Object,
    size: usize,
    range: &ByteRangeSpec,
    down: &mut Vec<u8>,
    completed: &[CompletedFilePart],
    is_head: bool,
) -> Result<usize, String> {
    info!("Download Range: {}", range);
    let mut read_size = 0;
    match range {
        ByteRangeSpec::FromTo(from, to) => {
            let from: usize = *from as usize;
            let to: usize = *to as usize;
            let mut so_far: usize = 0;
            for part in completed {
                if ((from >= part.part_start) && (from <= part.part_end)) && (to >= part.part_start)
                {
                    // Starts in this part
                    if to <= part.part_end {
                        // Ends in this part
                        if !is_head {
                            read_size += get_part(object, part, down, from - so_far, to - so_far, is_head)?;
                        }
                        return Ok(read_size);
                    } else {
                        // Ends in upcoming part
                        read_size += get_part(object, part, down, from - so_far, part.part_size - 1, is_head)?;
                    }
                } else if (from < part.part_start) && (to > part.part_end) {
                    // Ends in upcoming part
                    read_size += get_part(object, part, down, 0, part.part_size - 1, is_head)?;
                } else if (from < part.part_start)
                    && ((to >= part.part_start) && (to <= part.part_end))
                {
                    // Ends in this part
                    read_size += get_part(object, part, down, 0, to - so_far, is_head)?;
                    return Ok(read_size);
                }
                so_far += part.part_size
            }
            if to >= size {
                Ok(read_size)
            } else {
                warn!("bad");
                Err("Err".to_string())
            }
        }
        ByteRangeSpec::AllFrom(from) => {
            let from: usize = *from as usize;
            let mut so_far: usize = 0;
            for part in completed {
                if (from >= part.part_start) && (from <= part.part_end) {
                    read_size += get_part(object, part, down, from - so_far, part.part_size - 1, is_head)?;
                } else if from < part.part_start {
                    read_size += get_part(object, part, down, 0, part.part_size - 1, is_head)?;
                }
                so_far += part.part_size
            }
            Ok(read_size)
        }
        ByteRangeSpec::Last(to) => {
            let to: usize = *to as usize;
            let mut so_far: usize = 0;
            for part in completed {
                if to < part.part_start {
                    read_size += get_part(object, part, down, 0, part.part_size - 1, is_head)?;
                } else if to < part.part_end {
                    read_size += get_part(object, part, down, 0, to - so_far, is_head)?;
                } else {
                    return Err("Err".to_string());
                }
                so_far += part.part_size
            }
            Ok(read_size)
        }
    }
}

fn get_part(
    object: &Object,
    part: &CompletedFilePart,
    down: &mut Vec<u8>,
    start: usize,
    end: usize,
    is_head: bool,
) -> Result<usize, String> {
    info!(
        "GET_PART: {}    {}     {}-{}",
        object, part.part_number, start, end
    );
    let mut f = try(OpenOptions::new().read(true).open(format!(
        "{}{}/.{}.part{}",
        FS_ROOT_DIR, object.bucket, object.name, part.part_number
    )))?;

    try(f.seek(SeekFrom::Start(start as u64)))?;
    let read_size = end - start + 1;
    info!("READ CAPACITY: {}", read_size);
    if is_head {
        return Ok(read_size)
    }
    let mut vec: Vec<u8> = vec![0u8; end - start + 1];
    //Vec::with_capacity(end - start + 1);
    try(f.read_exact(&mut vec))?;
    info!("{}", vec.len());
    down.append(&mut vec);
    Ok(read_size)
}

//pub fn cleanup_part(part: &Part) -> () {
//    remove_part_file(part).unwrap_or(());
//}

fn remove_lock_file(object: &Object) -> Result<(), String> {
    let path = Object::get_lock_path(object);
    if path.is_file() {
        try(remove_file(path))?;
        Ok(())
    } else {
        Err("Lock file does not exist".to_string())
    }
}

#[allow(dead_code)]
fn remove_complete_file(object: &Object) -> Result<(), String> {
    let path = Object::get_complete_path(object);
    if path.is_file() {
        try(remove_file(path))?;
        Ok(())
    } else {
        Err("Complete file does not exist".to_string())
    }
}

fn part_exists(part: &Part) -> bool {
    let path = part.get_path();
    path.is_file()
}

fn object_name_valid(name: &str) -> bool {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^[a-zA-Z0-9\-_.]+$").unwrap();
    }
    RE.is_match(name) && !name.starts_with('.') && !name.ends_with('.')
}

fn part_number_in_range(number: u64) -> bool {
    (0 < number) && (number <= 10_000)
}

fn validate_bucket(bucket: &str) -> Result<(), String> {
    if !bucket_exists(bucket) {
        Err("InvalidBucket".to_string())
    } else {
        Ok(())
    }
}

fn object_is_valid(object: &Object) -> bool {
    bucket_exists(&object.bucket) && object_name_valid(&object.name)
}

fn validate_object(object: &Object) -> Result<(), String> {
    if !object_is_valid(object) {
        Err("InvalidBucket".to_string())
    } else {
        Ok(())
    }
}

fn get_modified_time<P: AsRef<Path>>(path: P) -> Result<u64, String> {
    let metadata = try(metadata(path))?;
    let time = try(metadata.modified())?;
    let time_since_epoch = try(time.duration_since(UNIX_EPOCH))?;
    Ok(time_since_epoch.as_secs() * 1000 + u64::from(time_since_epoch.subsec_millis()))
}

fn try<T, E: Display>(res: Result<T, E>) -> Result<T, String> {
    match res {
        Err(e) => Err(format!("{}", e)),
        Ok(t) => Ok(t),
    }
}

pub fn parse_hex(hex_str: &str) -> Vec<u8> {
    let mut hex_bytes = hex_str
        .as_bytes()
        .iter()
        .filter_map(|b| match b {
            b'0'...b'9' => Some(b - b'0'),
            b'a'...b'f' => Some(b - b'a' + 10),
            b'A'...b'F' => Some(b - b'A' + 10),
            _ => None,
        }).fuse();

    let mut bytes = Vec::new();
    while let (Some(h), Some(l)) = (hex_bytes.next(), hex_bytes.next()) {
        bytes.push(h << 4 | l)
    }
    bytes
}

pub fn to_hex_string(bytes: &[u8]) -> String {
    let string: Vec<String> = bytes.iter().map(|b| format!("{:02X}", b)).collect();
    string.join("")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn object_name() {
        assert_eq!(object_name_valid(""), false);
        assert_eq!(object_name_valid("a"), true);
        assert_eq!(object_name_valid("aa"), true);
        assert_eq!(object_name_valid("."), false);
        assert_eq!(object_name_valid(".aa"), false);
        assert_eq!(object_name_valid("aa."), false);
        assert_eq!(object_name_valid("a.a"), true);
    }

    #[test]
    fn part_number() {
        assert_eq!(part_number_in_range(0), false);
        assert_eq!(part_number_in_range(1), true);
        assert_eq!(part_number_in_range(10_000), true);
        assert_eq!(part_number_in_range(10_001), false);
    }
}
