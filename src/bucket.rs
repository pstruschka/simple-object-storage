extern crate fs2;
extern crate regex;

use self::fs2::FileExt;

use self::regex::Regex;
use bincode::{deserialize_from, serialize_into};
use mongodb::Client;
use object::FileHeader;
use serde_json::Value;
use std::fmt::Debug;
use std::fs::{self, rename, OpenOptions};
use std::io::{Seek, SeekFrom};
use std::path::{Path, PathBuf};
use std::time::{SystemTime, UNIX_EPOCH};

use mongo_middleware::drop_bucket_metadata;
use object::to_hex_string;

static FS_ROOT_DIR: &'static str = "./store/";
static BUCKET_NAME_REGEX: &'static str = r"^[a-zA-Z0-9\-_]+$";

#[repr(align(8))]
#[derive(Copy, Serialize, Deserialize, Debug)]
pub struct BucketMetadata {
    pub created: u64,
    pub modified: u64,
}

impl Clone for BucketMetadata {
    fn clone(&self) -> BucketMetadata {
        *self
    }
}

/// Creates a directory which corresponds to a bucket
pub fn create_bucket(bucket: &str) -> Result<Value, String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(BUCKET_NAME_REGEX).unwrap();
    }
    if !RE.is_match(bucket) {
        return Err("Invalid Name".to_string());
    }

    let bucket_path = format!("{}{}/completed", FS_ROOT_DIR, bucket);

    try(fs::create_dir_all(&bucket_path))?;

    let bucket_metadata_path = format!("{}{}/.metadata", FS_ROOT_DIR, bucket);

    let f = try(OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(PathBuf::from(&bucket_metadata_path)))?;
    let modified = get_modified_time(bucket_metadata_path)?;

    let bucket_metadata = BucketMetadata {
        created: modified,
        modified,
    };
    try(serialize_into(f, &bucket_metadata))?;

    Ok(json!({
        "created": bucket_metadata.created,
        "modified": bucket_metadata.modified,
        "name": bucket
    }))
}

pub fn drop_bucket(bucket: &str, client: &Client) -> Result<(), String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(BUCKET_NAME_REGEX).unwrap();
    }
    if !RE.is_match(bucket) {
        return Err("Invalid Name".to_string());
    }

    let bucket_path = format!("{}{}", FS_ROOT_DIR, bucket);

    let path = Path::new(&bucket_path);

    if path.is_dir() {
        let time = SystemTime::now();
        let time_since_epoch = try(time.duration_since(UNIX_EPOCH))?;
        let time = time_since_epoch.as_secs() * 1000 + u64::from(time_since_epoch.subsec_millis());
        try(rename(path, format!("{}{}-{}", FS_ROOT_DIR, bucket, time)))?;
        //try(fs::remove_dir_all(path))?;

        drop_bucket_metadata(bucket, client)?;

        Ok(())
    } else {
        Err("Bucket does not exist".to_string())
    }
}

pub fn list_bucket(bucket: &str) -> Result<Value, String> {
    let bucket_metadata_str = format!("{}{}/.metadata", FS_ROOT_DIR, bucket);
    let bucket_metadata_path = PathBuf::from(&bucket_metadata_str);
    if !bucket_metadata_path.is_file() {
        return Err("Bucket does not exist".to_string());
    }
    let f = try(OpenOptions::new()
        .read(true)
        .open(PathBuf::from(&bucket_metadata_path)))?;
    let bucket_metadata: BucketMetadata = try(deserialize_from(f))?;
    let bucket_path = format!("{}{}/completed", FS_ROOT_DIR, bucket);

    let path = Path::new(&bucket_path);
    if !path.is_dir() {
        return Err("Bucket does not exist".to_string());
    }
    let dir = try(path.read_dir())?;

    let mut objects: Vec<Value> = vec![];
    for entry in dir {
        if let Ok(entry) = entry {
            let completed_name: String = try(entry.file_name().into_string())?;
            let (object_name, _) = completed_name.as_str().split_at(completed_name.len() - 10);

            let mut f = try(OpenOptions::new()
                .write(true)
                .read(true)
                .open(&entry.path()))?;
            let header: FileHeader = try(deserialize_from(&f))?;

            info!("file: {:?}, path: {:?}", entry.file_name(), entry.path());
            //let time = get_modified_time(entry.path())?;
            let v = json!({
                "name": object_name,
                "eTag": format!("{}-{}", to_hex_string(&header.md5), header.number_of_parts),
                "created": header.created,
                "modified": header.modified,
            });
            objects.push(v);
        }
    }

    Ok(json!({
        "created": bucket_metadata.created,
        "modified": bucket_metadata.modified,
        "name": *bucket,
        "objects": objects
        }))
}

pub fn bucket_exists(bucket: &str) -> bool {
    Path::new(&format!("{}{}", FS_ROOT_DIR, bucket)).is_dir()
}

pub fn update_bucket_modified(bucket: &str, time: u64) -> Result<(), String> {
    let bucket_metadata_str = format!("{}{}/.metadata", FS_ROOT_DIR, bucket);
    let bucket_metadata_path = PathBuf::from(&bucket_metadata_str);
    if !bucket_metadata_path.is_file() {
        return Err("Bucket does not exist".to_string());
    }
    let mut f = try(OpenOptions::new()
        .write(true)
        .read(true)
        .open(PathBuf::from(&bucket_metadata_path)))?;
    try(f.lock_exclusive())?;
    let mut bucket_metadata: BucketMetadata = try(deserialize_from(&f))?;

    try(f.seek(SeekFrom::Start(0)))?;

    bucket_metadata.modified = time;

    try(serialize_into(&f, &bucket_metadata))?;

    try(f.unlock())?;

    Ok(())
}

/// Internal function to convert a result to the correct
/// form required
fn try<T, E: Debug>(res: Result<T, E>) -> Result<T, String> {
    match res {
        Err(e) => Err(format!("{:?}", e)),
        Ok(t) => Ok(t),
    }
}

fn get_modified_time<P: AsRef<Path>>(path: P) -> Result<u64, String> {
    let metadata = try(fs::metadata(path))?;
    let time = try(metadata.modified())?;
    let time_since_epoch = try(time.duration_since(UNIX_EPOCH))?;
    Ok(time_since_epoch.as_secs() * 1000 + u64::from(time_since_epoch.subsec_millis()))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn name_filter() {
        let name_error_create: Result<Value, String> = Err("Invalid Name".to_string());
        assert_eq!(create_bucket("."), name_error_create);
        assert_eq!(create_bucket("abc&"), name_error_create);
    }
}
