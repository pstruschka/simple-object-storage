//#![feature(tool_lints)]

extern crate iron;
#[macro_use]
extern crate router;
#[macro_use]
extern crate serde_json;
#[macro_use]
extern crate serde_derive;
extern crate urlencoded;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;
#[macro_use(bson, doc)]
extern crate bson;
extern crate bincode;
extern crate flexi_logger;
extern crate logger;
extern crate mongodb;

use flexi_logger::with_thread;
use iron::prelude::*;
use logger::{Format, Logger};
use std::{thread, time};

mod bucket;
mod handlers;
mod logging_middleware;
mod metadata_handlers;
mod mongo_middleware;
mod object;
mod object_handlers;

use handlers::{
    create_bucket_handler, delete_object_handler, drop_bucket_handler, get_object_handler,
    list_bucket_handler, object_ticket_handler, put_object_handler,
};

static FORMAT: &'static str = "{method} {uri} -> {status} ({response-time})";

fn main() {
    flexi_logger::Logger::with_env_or_str("info")
        .log_to_file()
        .duplicate_to_stderr(flexi_logger::Duplicate::Info)
        .format(with_thread)
        .start()
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));

    info!("Sleeping for database init");
    let three_seconds = time::Duration::from_secs(3);
    thread::sleep(three_seconds);
    info!("Starting");

    let router = router! {
        create_bucket_route: post   "/:bucket" => create_bucket_handler,
        drop_bucket_route:   delete "/:bucket" => drop_bucket_handler,
        list_bucket_route:   get    "/:bucket" => list_bucket_handler,

        object_ticket_route: post   "/:bucket/:object" => object_ticket_handler,
        get_object_route:    get    "/:bucket/:object" => get_object_handler,
        get_object_route:    head   "/:bucket/:object" => get_object_handler,
        put_object_route:    put    "/:bucket/:object" => put_object_handler,
        delete_object_route: delete "/:bucket/:object" => delete_object_handler
    };

    let mut chain = Chain::new(router);
    let format = Format::new(FORMAT);
    chain.link(Logger::new(format));
    chain.link_before(mongo_middleware::MongoMiddleware::new("db", 27017).unwrap());
    chain.link_before(logging_middleware::LogPathHandler);

    info!("Serving on http://localhost:5000!");
    Iron::new(chain).http("0.0.0.0:5000").unwrap();
}
