use iron::error;
use iron::headers::parsing::from_one_raw_str;
use iron::headers::{ByteRangeSpec, ContentLength, Range, ETag, EntityTag};
use iron::headers::{Header, HeaderFormat};
use iron::mime::Mime;
use iron::method::{Head};
use iron::status;
use iron::Plugin;
use iron::{IronResult, Request, Response};
use router::Router;
use std::fmt;
use std::ops::Deref;
use urlencoded::{QueryMap, UrlEncodedQuery};

use bucket::{create_bucket, drop_bucket, list_bucket};
use object::{parse_hex, Object, Part};

use object::CompleteObject;
use object_handlers::{complete_ticket, create_ticket, delete_part, download_range, upload_part};

use metadata_handlers::{delete_metadata, get_all_metadata, get_metadata, put_metadata};
use object_handlers::delete_object;

use mongo_middleware::MongoRequestExtension;

#[derive(Clone, Debug, PartialEq)]
struct ContentMd5(String);

impl Header for ContentMd5 {
    fn header_name() -> &'static str {
        "Content-MD5"
    }

    fn parse_header(raw: &[Vec<u8>]) -> Result<ContentMd5, error::HttpError> {
        from_one_raw_str(raw).map(ContentMd5)
    }
}

impl HeaderFormat for ContentMd5 {
    fn fmt_header(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&*self, f)
    }
}

impl fmt::Display for ContentMd5 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&*self.0, f)
    }
}

pub fn create_bucket_handler(req: &mut Request) -> IronResult<Response> {
    let query = match req.url.query() {
        None => return Ok(Response::with(status::BadRequest)),
        Some(s) => s,
    };

    match query {
        "create" => {
            let bucket = req
                .extensions
                .get::<Router>()
                .unwrap()
                .find("bucket")
                .unwrap_or("/");

            let mime = "application/json".parse::<Mime>().unwrap();
            match create_bucket(bucket) {
                Ok(json) => Ok(Response::with((mime, status::Ok, json.to_string()))),
                Err(e) => {
                    warn!("{}", e);
                    Ok(Response::with(status::BadRequest))
                }
            }
        }
        _ => Ok(Response::with(status::BadRequest)),
    }
}

pub fn drop_bucket_handler(req: &mut Request) -> IronResult<Response> {
    let query = match req.url.query() {
        None => return Ok(Response::with(status::BadRequest)),
        Some(s) => s,
    };

    match query {
        "delete" => {
            let bucket = req
                .extensions
                .get::<Router>()
                .unwrap()
                .find("bucket")
                .unwrap_or("/");
            match drop_bucket(bucket, req.database_connection()) {
                Ok(_) => Ok(Response::with(status::Ok)),
                Err(e) => {
                    warn!("{}", e);
                    Ok(Response::with(status::BadRequest))
                }
            }
        }
        _ => Ok(Response::with(status::BadRequest)),
    }
}

pub fn list_bucket_handler(req: &mut Request) -> IronResult<Response> {
    let map = match req.get::<UrlEncodedQuery>() {
        Err(ref e) => {
            warn!("{}", e);
            return Ok(Response::with(status::BadRequest));
        }
        Ok(map) => map,
    };

    match filter_url_query(&map, &["list"]) {
        Some(_) => {
            let bucket = req
                .extensions
                .get::<Router>()
                .unwrap()
                .find("bucket")
                .unwrap_or("/");

            let mime = "application/json".parse::<Mime>().unwrap();
            match list_bucket(bucket) {
                Ok(r) => Ok(Response::with((mime, status::Ok, r.to_string()))),
                Err(e) => {
                    warn!("{}", e);
                    Ok(Response::with(status::BadRequest))
                }
            }
        }
        _ => Ok(Response::with(status::BadRequest)),
    }
}

pub fn object_ticket_handler(req: &mut Request) -> IronResult<Response> {
    let query = match req.url.query() {
        Some(s) => s,
        None => return Ok(Response::with(status::BadRequest)),
    };

    let object = match get_bucket_from_request_path(&req) {
        Some(bucket) => bucket,
        None => return Ok(Response::with(status::BadRequest)),
    };

    info!("Get {}", object);

    match query {
        "create" => Ok(Response::with(create_ticket(&object))),
        "complete" => {
            let e_tag: String = match req.headers.get::<ContentMd5>() {
                Some(md5) => md5.to_string(),
                None => "eTag".to_string()//return Ok(Response::with(status::BadRequest)),
            };

            let complete: CompleteObject = CompleteObject {
                object,
                total_length: 0,
                e_tag,
            };
            let mime = "application/json".parse::<Mime>().unwrap();
            let (json, status) = complete_ticket(&complete);
            Ok(Response::with((mime, status, json.to_string())))
        }
        _ => Ok(Response::with(status::BadRequest)),
    }
}

pub fn get_object_handler(req: &mut Request) -> IronResult<Response> {
    let object = match get_bucket_from_request_path(&req) {
        Some(bucket) => bucket,
        None => return Ok(Response::with(status::BadRequest)),
    };

    let map = match req.get::<UrlEncodedQuery>() {
        Err(ref _e) => {
            let is_head = match req.method {
                Head => true,
                _ => false,
            };
            info!("head {} {}", req.method, is_head);
            let range = match req.headers.get::<Range>() {
                Some(r) => r,
                None => {
                    info!("no range");
                    let range = Range::Bytes(vec![ByteRangeSpec::AllFrom(0)]);
                    match download_range(&object, &range, is_head) {
                        (Some(t), l, e, s) => {
                            let data = t.into_vec();
                            let mut resp = Response::with((s, data));
                            resp.headers.set(ContentLength(l as u64));
                            resp.headers.set(ETag(EntityTag::new(true, e.to_owned())));
                            return Ok(resp);
                        }
                        (None, l, e, s) => {
                            let mut resp = Response::with((s, vec![]));
                            resp.headers.set(ContentLength(l as u64));
                            resp.headers.set(ETag(EntityTag::new(true, e.to_owned())));
                            return Ok(resp);
                        }
                    }
                }
            };
            match download_range(&object, range, is_head) {
                (Some(t), l, e, s) => {
                    let data = t.into_vec();
                    let mut resp = Response::with((s, data));
                    resp.headers.set(ContentLength(l as u64));
                    resp.headers.set(ETag(EntityTag::new(true, e.to_owned())));
                    return Ok(resp);
                }
                (None, l, e, s) => {
                    let mut resp = Response::with((s, vec![]));
                    resp.headers.set(ContentLength(l as u64));
                    resp.headers.set(ETag(EntityTag::new(true, e.to_owned())));
                    return Ok(resp);
                }
            }
        }
        Ok(map) => map,
    };

    if let Some(key) = filter_url_query(&map, &["metadata", "key"]) {
        let mime = "application/json".parse::<Mime>().unwrap();
        let (json, status) = get_metadata(&object, &key, req);
        match status {
            status::Ok => Ok(Response::with((mime, status, json.to_string()))),
            _ => Ok(Response::with(status)),
        }
    } else if filter_url_query(&map, &["metadata"]).is_some() {
        let mime = "application/json".parse::<Mime>().unwrap();
        let (json, status) = get_all_metadata(&object, req);
        match status {
            status::Ok => Ok(Response::with((mime, status, json.to_string()))),
            _ => Ok(Response::with(status)),
        }
    } else {
        Ok(Response::with(status::NotFound))
    }
}

pub fn put_object_handler(req: &mut Request) -> IronResult<Response> {
    let map = match req.get::<UrlEncodedQuery>() {
        Err(_) => {
            return Ok(Response::with(status::NotFound));
        }
        Ok(map) => map,
    };

    let object = match get_bucket_from_request_path(&req) {
        Some(bucket) => bucket,
        None => return Ok(Response::with(status::BadRequest)),
    };

    if let Some(key) = filter_url_query(&map, &["metadata", "key"]) {
        return Ok(Response::with(put_metadata(&object, &key, req)));
    }
    match filter_url_query(&map, &["partNumber"]) {
        Some(number) => {
            let mime = "application/json".parse::<Mime>().unwrap();

            let content_size: usize = match req.headers.get::<ContentLength>() {
                Some(size) => *size.deref() as usize,
                None => return Ok(Response::with(status::BadRequest)),
            };

            let part_md5_str: String = match req.headers.get::<ContentMd5>() {
                Some(md5) => md5.to_string(),
                None => {
                    return Ok(Response::with((
                        mime,
                        status::BadRequest,
                        json!({
                    "error": "MissingHeaderContentMD5"
                }).to_string(),
                    )))
                }
            };

            let part_md5 = parse_hex(&part_md5_str);

            if (part_md5_str.len() != 32) || (part_md5.len() != 16) {
                return Ok(Response::with((
                    mime,
                    status::BadRequest,
                    json!({
                    "error": "InvalidMD5"
                }).to_string(),
                )));
            }

            let mut md = [0u8; 16];
            md.copy_from_slice(&part_md5);

            info!("PUT {} part {}", object, number);

            let part = Part {
                object,
                number: number.parse().unwrap(),
                size: content_size,
                md5: md,
            };

            let (json, status) = upload_part(&part, &mut req.body);
            Ok(Response::with((mime, status, json.to_string())))
        }
        _ => Ok(Response::with(status::BadRequest)),
    }
}

pub fn delete_object_handler(req: &mut Request) -> IronResult<Response> {
    let map = match req.get::<UrlEncodedQuery>() {
        Err(_) => return Ok(Response::with(status::BadRequest)),
        Ok(map) => map,
    };

    let object = match get_bucket_from_request_path(&req) {
        Some(bucket) => bucket,
        None => return Ok(Response::with(status::BadRequest)),
    };

    if let Some(v) = filter_url_query(&map, &["delete"]) {
        if v.is_empty() {
            delete_object(&object, req.database_connection());
            return Ok(Response::with(status::Ok));
        } else {
            return Ok(Response::with(status::BadRequest));
        }
    }

    if let Some(number) = filter_url_query(&map, &["partNumber"]) {
        let part = Part {
            object,
            number: number.parse().unwrap(),
            size: 0,
            md5: [0u8; 16],
        };

        return Ok(Response::with(delete_part(&part)));
    }

    match filter_url_query(&map, &["metadata", "key"]) {
        Some(key) => Ok(Response::with(delete_metadata(&object, &key, req))),
        _ => Ok(Response::with(status::BadRequest)),
    }
}

fn filter_url_query(map: &QueryMap, args: &[&str]) -> Option<String> {
    // Checks that a QueryMap contains all the specified arguments and returns
    // the value of the name argument
    let mut result: Option<String> = None;
    if map.len() != args.len() {
        return None;
    }

    for (counter, arg) in args.into_iter().enumerate() {
        if !map.contains_key(*arg) {
            return None;
        }
        if counter == args.len() - 1 {
            match map.get(*arg) {
                None => {
                    return None;
                }
                Some(t) => {
                    if t.len() == 1 {
                        result = Some(t[0].clone());
                    } else {
                        return None;
                    }
                }
            }
        }
    }
    result
}

fn get_bucket_from_request_path(req: &Request) -> Option<Object> {
    let bucket = req.extensions.get::<Router>().unwrap().find("bucket")?;
    let object = req.extensions.get::<Router>().unwrap().find("object")?;

    Some(Object {
        bucket: bucket.to_string(),
        name: object.to_string(),
    })
}
