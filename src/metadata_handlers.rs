use std::io::Read;
use std::ops::Deref;

use iron::headers::ContentLength;
use iron::request::Request;
use iron::status::{self, Status};
use mongo_middleware::MongoRequestExtension;
use mongodb::coll::options::UpdateOptions;
use mongodb::db::ThreadedDatabase;
use mongodb::ThreadedClient;
use serde_json::Value;

use mongo_middleware::DATABASE;
use object::Object;

pub fn put_metadata(object: &Object, key: &str, req: &mut Request) -> Status {
    if object.is_complete() {
        let content_size: usize = match req.headers.get::<ContentLength>() {
            Some(size) => *size.deref() as usize,
            None => 0,
        };

        let mut value = String::with_capacity(content_size);
        {
            let body = &mut req.body;
            if let Err(e) = body.read_to_string(&mut value) {
                warn!("{}", e);
                return status::InternalServerError;
            }
        }

        let client = req.database_connection();

        let coll = client.db(DATABASE).collection(&object.bucket);

        match coll.update_one(
            doc!{
                object.to_base64(): {"$exists": true}
            },
            doc!{
                "$set": {
                    format!("{}.{}", object.to_base64(), key): value
                }
            },
            Some(UpdateOptions {
                upsert: Some(true),
                write_concern: None,
            }),
        ) {
            Ok(_) => info!("{} METADATA:{} UPDATED", object, key),
            Err(e) => warn!("{} METADATA:{} UPDATE FAILED {}", object, key, e),
        }
        status::Ok
    } else {
        status::NotFound
    }
}
pub fn delete_metadata(object: &Object, key: &str, req: &mut Request) -> Status {
    if object.is_complete() {
        let client = req.database_connection();
        let coll = client.db(DATABASE).collection(&object.bucket);
        match coll.update_one(
            doc!{
                object.to_base64(): {"$exists": true}
            },
            doc!{
                "$unset": {
                    format!("{}.{}", object.to_base64(), key): ""
                }
            },
            None,
        ) {
            Ok(_) => info!("{} METADATA:{} UPDATED", object, key),
            Err(e) => warn!("{} METADATA:{} UPDATE FAILED {}", object, key, e),
        }
        status::Ok
    } else {
        status::NotFound
    }
}
pub fn get_metadata(object: &Object, key: &str, req: &mut Request) -> (Value, Status) {
    if object.is_complete() {
        let client = req.database_connection();
        let coll = client.db(DATABASE).collection(&object.bucket);
        match coll.find_one(
            Some(doc!{
                object.to_base64(): {"$exists": true}
            }),
            None,
        ) {
            Ok(doc) => match doc {
                Some(obj) => match obj.get_document(&object.to_string()) {
                    Ok(meta) => match meta.get_str(key) {
                        Ok(val) => return (json!({ key: val }), status::Ok),
                        Err(e) => {
                            warn!("{}", e);
                            return (json!({}), status::Ok);
                        }
                    },
                    Err(e) => {
                        warn!("{}", e);
                    }
                },
                None => return (json!({}), status::Ok),
            },
            Err(e) => {
                warn!("{}", e);
            }
        }
        (json!({}), status::InternalServerError)
    } else {
        (json!({}), status::NotFound)
    }
}
pub fn get_all_metadata(object: &Object, req: &mut Request) -> (Value, Status) {
    if object.is_complete() {
        let client = req.database_connection();
        let coll = client.db(DATABASE).collection(&object.bucket);
        if let Ok(cursor) = coll.find(
            Some(doc!{
                object.to_base64(): {"$exists": true}
            }),
            None,
        ) {
            for item in cursor {
                match item {
                    Ok(doc) => match doc.get_document(&object.to_base64()) {
                        Ok(keys) => {
                            info!("{}", keys);
                            return (json!(keys), status::Ok);
                        }
                        Err(e) => warn!("{}", e),
                    },
                    Err(e) => {
                        error!("{}", e);
                    }
                }
            }
            (json!({}), status::Ok)
        } else {
            (json!({}), status::InternalServerError)
        }
    } else {
        (json!({}), status::NotFound)
    }
}
