FROM rust:1.29.0 as build

# create a new empty shell project
RUN USER=root cargo new --bin simple-object-storage
WORKDIR /simple-object-storage

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src

# RUN cargo clean

# build for release
RUN touch ./src/main.rs
RUN cargo build --release

FROM rust:1.29.0

COPY --from=build /simple-object-storage/target/release/simple-object-storage .
RUN mkdir store

ENV RUST_LOG info
CMD ["./simple-object-storage"]
